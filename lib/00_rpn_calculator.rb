class RPNCalculator
  # TODO: your code goes here!

  def initialize
    @array = []
  end

  def push(number)
    @array.push(number)
  end

  def pop(number)
    @array.pop(number)
  end

  def plus
    raise "calculator is empty" if @array.empty?
    num2 = @array.pop
    num1 = @array.pop
    @array.push(num1 + num2)
  end

  def minus
    raise "calculator is empty" if @array.empty?
    num2 = @array.pop
    num1 = @array.pop
    @array.push(num1 - num2)
  end

  def divide
    raise "calculator is empty" if @array.empty?
    num2 = @array.pop
    num1 = @array.pop
    @array.push(num1.to_f / num2)
  end

  def times
    raise "calculator is empty" if @array.empty?
    num2 = @array.pop
    num1 = @array.pop
    @array.push(num1 * num2)
  end

  def tokens(string)
    tokens = string.split
    tokens.map! do |tkn|
      if /\d/ =~ tkn
        tkn.to_i
      else
        tkn.to_sym
      end
    end
  end

  def evaluate(string)
    tokens = tokens(string)
    tokens.each do |tkn|
      if tkn.is_a? Integer
        push(tkn)
      else
        operand(tkn)
      end
    end
    
    value
  end

  def operand(symbol)
    if symbol == :+
      plus
    elsif symbol == :-
      minus
    elsif symbol == :/
      divide
    elsif symbol == :*
      times
    end
  end

  def value
    @array.last
  end
end
